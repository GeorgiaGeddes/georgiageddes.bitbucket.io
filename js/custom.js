document.querySelector('.hamburger').addEventListener('click', function() {
    document.querySelector('.nav').classList.toggle('open');
});

var $grid = $('.grid').imagesLoaded( function() {
    $grid.isotope({
        itemSelector: '.grid-item',
        //percentPosition: true,//
        masonry: {
          columnWidth: 200,
          isFitWidth: true,
        }
    });
    $('.grid').css('opacity',1);
});